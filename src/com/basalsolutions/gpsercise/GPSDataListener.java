package com.basalsolutions.gpsercise;

public interface GPSDataListener {
	public abstract void onGPSData(Workout workout);
}
