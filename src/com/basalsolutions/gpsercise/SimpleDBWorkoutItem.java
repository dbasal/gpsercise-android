package com.basalsolutions.gpsercise;

public class SimpleDBWorkoutItem {

	private String _workoutid;
	private String _startTime;
	private String _endTime;
	private String _pausedTime;
	private String _distance;
	private String _lat;
	private String _lon;
	private String _time;
	private String _accuracy;
	private String _bearing;
	private String _altitude;
	private String _isSplit;
	private String _splitDistance;
	private String _elapsedTime;
	
	public String get_pausedTime() {
		return _pausedTime;
	}

	public void set_pausedTime(String _pausedTime) {
		this._pausedTime = _pausedTime;
	}
	
	public String get_workoutid() {
		return _workoutid;
	}

	public void set_workoutid(String _workoutid) {
		this._workoutid = _workoutid;
	}

	public String get_startTime() {
		return _startTime;
	}

	public void set_startTime(String _startTime) {
		this._startTime = _startTime;
	}

	public String get_endTime() {
		return _endTime;
	}

	public void set_endTime(String _endTime) {
		this._endTime = _endTime;
	}

	public String get_distance() {
		return _distance;
	}

	public void set_distance(String _distance) {
		this._distance = _distance;
	}

	public String get_lat() {
		return _lat;
	}

	public void set_lat(String _lat) {
		this._lat = _lat;
	}

	public String get_lon() {
		return _lon;
	}

	public void set_lon(String _lon) {
		this._lon = _lon;
	}

	public String get_time() {
		return _time;
	}

	public void set_time(String _time) {
		this._time = _time;
	}

	public String get_accuracy() {
		return _accuracy;
	}

	public void set_accuracy(String _accuracy) {
		this._accuracy = _accuracy;
	}

	public String get_bearing() {
		return _bearing;
	}

	public void set_bearing(String _bearing) {
		this._bearing = _bearing;
	}

	public String get_altitude() {
		return _altitude;
	}

	public void set_altitude(String _altitude) {
		this._altitude = _altitude;
	}

	public String get_isSplit() {
		return _isSplit;
	}

	public void set_isSplit(String _isSplit) {
		this._isSplit = _isSplit;
	}

	public String get_splitDistance() {
		return _splitDistance;
	}

	public void set_splitDistance(String _splitDistance) {
		this._splitDistance = _splitDistance;
	}

	public SimpleDBWorkoutItem() {
	}

	public String get_elapsedTime() {
		return _elapsedTime;
	}

	public void set_elapsedTime(String _elapsedTime) {
		this._elapsedTime = _elapsedTime;
	}

}
