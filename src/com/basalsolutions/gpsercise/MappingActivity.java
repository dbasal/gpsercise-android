package com.basalsolutions.gpsercise;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.RemoteViews;
import android.widget.TextView;

public class MappingActivity extends Activity implements GPSDataListener, TimeListener, OnClickListener {

	private GoogleMap _map = null;
	private PolylineOptions _points = new PolylineOptions();
	private GPSController _gpsController = null;
	private static final int _ZOOMFACTOR = 15;
	private Circle _currentLocation = null;
	private Chronometer _crSplitTime;
	private Chronometer _crOverallTime;
	private Button _btnControl;
	private TextView _tvDistance;
	private TextView _tvAvgSpeed;
	private TextView _tvCurrentSpeed;
	private boolean _firstFix = true;
	private int _pointsDisplayed = 0;
	private int _workoutindex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		setup();
	}

	/* BroadcastReceiver to receive the new workout information from the workoutcontroller */
	private BroadcastReceiver oneShotLocationReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// Returned in the intent is an arraylist of location infos
			LocationInfo locinfo = (LocationInfo) intent.getParcelableExtra("location");
			mapPoint(locinfo);
		}
	};

	/* BroadcastReceiver to receive the new workout information from the workoutcontroller */
	private BroadcastReceiver messageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// Returned in the intent is an arraylist of location infos
			Workout wrk = (Workout) intent.getParcelableExtra("workout");
			updateMap(wrk);
		}
	};

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	private void setup() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity.setup +++++");
		
		_btnControl = (Button) findViewById(R.id.btnMapControl);
		_btnControl.setOnClickListener(this);

		_crOverallTime = (Chronometer) findViewById(R.id.crMapOverallTime);
		_crSplitTime = (Chronometer) findViewById(R.id.crMapSplitTime);

		_tvAvgSpeed = (TextView) findViewById(R.id.txtAvgSpeedPaceValue);
		_tvCurrentSpeed = (TextView) findViewById(R.id.txtCurrentSpeedPaceValue);
		_tvDistance = (TextView) findViewById(R.id.txtMapDistance);

		// Check and make sure we need to get the map
		if (_map == null) {
			_map = ((MapFragment) getFragmentManager().findFragmentById(R.id.gpsercisemap)).getMap();

			if (_map == null) {
				try {
					throw new Exception("MAP IS NULL");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		// Need to set the current position of the map
		_map.setMyLocationEnabled(false);

		// Get the GPSController we need for this view
		_gpsController = GPSController.getInstance(this);

		/*
		 * if (!_gpsController.is_collectingdata()) { _gpsController.startGPSService(2); } else { resume(); }
		 */
		// _gpsController.setGPSDataListener(this);

		_gpsController.set_timeListener(this);
		
		if (readStateFile()) {
			// Need to let the GPS controller that we need the data, stat!
			_gpsController.broadcastWorkout();
		}
		
		LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter("new-workout-data"));
		LocalBroadcastManager.getInstance(this).registerReceiver(oneShotLocationReceiver, new IntentFilter("one-shot-location"));
		FileLogger.writeToFile("MappingActivity", "----- MappingActivity.setup -----");
	}

	public void replay(ArrayList<LocationInfo> locinfolist) {
		for (int i = 0; i < locinfolist.size(); i++) {
			LocationInfo locinfo = locinfolist.get(i);

			LatLng latlng = new LatLng(locinfo.getLatitude(), locinfo.getLongitude());

			addNewPoint(latlng);
		}
	}

	private void resume() {
		// The first thing that must be done is to get the map back to the proper display state

	}

	private Polyline addNewPoint(LatLng latlng) {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity addNewPoint +++++");

		_pointsDisplayed++;

		FileLogger.writeToFile("MappingActivity", "_pointsDisplayed " + _pointsDisplayed);
		// Draw the circle to represent the current location
		FileLogger.writeToFile("MappingActivity", "Creating new CircleOptions to mark current position");
		CircleOptions co = new CircleOptions().center(latlng).fillColor(Color.BLUE).radius(3);

		FileLogger.writeToFile("MappingActivity", "Checking to see if _currentLocation != null");

		// First be sure to remove the previous location if one exists
		if (_currentLocation != null) {
			FileLogger.writeToFile("MappingActivity", "_currentLocation != null. Remove it from the map");
			_currentLocation.remove();
		}

		FileLogger.writeToFile("MappingActivity", "Adding new circle to the map and setting _currentLocation with it");
		_currentLocation = _map.addCircle(co);

		FileLogger.writeToFile("MappingActivity", "Adding point to the PolyLineOptions to draw the line on the map");

		// Draw the line
		_points.add(latlng).width(10).color(Color.RED);

		FileLogger.writeToFile("MappingActivity", "---- MappingActivity addNewPoint -----");

		return _map.addPolyline(_points);
	}

	private void addTerminus(LatLng latlng, int color) {
		CircleOptions co = new CircleOptions().center(latlng).radius(10).fillColor(color);

		_map.addCircle(co);

		// Add the new point to the polylineOptions instance
		_points = new PolylineOptions().add(latlng);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public void onGPSData(Workout workout) {
		// Check to see if this is the first fix received.
		// If so, then start the chronos
		if (_firstFix) {
			_firstFix = false;
			initializeControls();
		}

		updateMap(workout);
		updateData(workout);
	}

	private void updateData(Workout workout) {
		double distance = GPSData.metersToMiles(workout.get_distance());

		_tvDistance.setText(String.format("%.2f", distance) + " " + AppPreferences.get_measureFormatString(AppPreferences.MEASUREFORMAT_MI));

		double avgspeed = workout.get_averageSpeed();

		_tvAvgSpeed.setText(String.format("%.2f", avgspeed) + " " + AppPreferences.get_paceString(AppPreferences.MEASUREFORMAT_MI));

		// The current speed would come from the last locationinfo object as it is not part of the workout object
		double currspeed = GPSData.meterSpeedToMilesSpeed(workout.getLastLocation().getSpeed());

		_tvCurrentSpeed.setText(String.format("%.2f", currspeed) + " " + AppPreferences.get_paceString(AppPreferences.MEASUREFORMAT_MI));
	}

	private void mapPoint(LocationInfo locinfo) {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity mapPoint +++++");

		LatLng latlng = new LatLng(locinfo.getLatitude(), locinfo.getLongitude());

		// Move the camera
		_map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, _ZOOMFACTOR));

		FileLogger.writeToFile("MappingActivity", "MappingActivity.updateMap : plotting latlng " + latlng.latitude + ":" + latlng.longitude);

		addNewPoint(latlng);
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity mapPoint +++++");
	}

	private void updateMap(Workout workout) {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity updateMap +++++");

		FileLogger.writeToFile("MappingActivity", "MappingActivity.updateMap : _workoutindex = " + _workoutindex);

		// Check to see if the workoutindex is 0. If it is, then we need to iterate through all locations and map them
		if (_workoutindex == 0) {
			ArrayList<LocationInfo> locinfos = workout.getLocations();

			for (LocationInfo locinfo : locinfos) {
				mapPoint(locinfo);
				_workoutindex++;
			}

		} else {
			/*
			 * ASSUMPTION ALERT: If the _workoutindex is not 0, then we only need the last location
			 */
			LocationInfo loc = workout.getLastLocation();
			mapPoint(loc);
		}

		// Once we have updated the map, then we can call the updateData. We do not need to constantly call updateData
		// with every point plotted
		updateData(workout);

		updateChronos(workout.get_baseTime(), workout.get_splitBaseTime());

		FileLogger.writeToFile("MappingActivity", "----- MappingActivity updateMap -----");
	}

	private void updateChronos(long basetime, long splitbasetime) {
		_crOverallTime.stop();
		_crOverallTime.setBase(basetime);
		_crOverallTime.start();

		_crSplitTime.stop();
		_crSplitTime.setBase(splitbasetime);
		_crSplitTime.start();

	}

	private void initializeControls() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity initializeControls() +++++");

		long millis = SystemClock.elapsedRealtime();
		_gpsController.set_baseTime(millis);
		_gpsController.set_splitBaseTime(millis);

		FileLogger.writeToFile("MappingActivity", "----- MappingActivity initializeControls() -----");
	}

	@Override
	public void onBaseTimeChange() {
		_crOverallTime.stop();
		_crOverallTime.setBase(_gpsController.get_baseTime());
		_crOverallTime.start();
	}

	@Override
	public void onSplitBaseTimeChange() {
		_crSplitTime.stop();
		_crSplitTime.setBase(_gpsController.get_splitBaseTime());
		_crSplitTime.start();
	}

	@Override
	public void onClick(View v) {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity onClick() +++++");

		if (_btnControl.getText().toString().equalsIgnoreCase("Stop")) {
			FileLogger.writeToFile("MappingActivity", "********* USER HIT STOP BUTTON *********");
			// We have a user that has clicked a "stop" button
			// Change the text back to start
			_btnControl.setText("Start");

			final long pausedat = SystemClock.elapsedRealtime();
			// _tvPaused.setText("Pause Time: " + _pausedat);
			_gpsController.stopLocationCollection();
			_crOverallTime.stop();
			_crSplitTime.stop();

			// Need to check with the user if they wish to pause or stop
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Workout stopped. Please select option").setCancelable(false)
					.setPositiveButton("Resume", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// Just let the place know we are pausing,
							// but will continue
							// When the user clicks this button, we must
							// change the base time for the chronos to
							// make sure they are keeping track properly
							FileLogger.writeToFile("MappingActivity", "********* USER SELECTED TO RESUME *********");
							long timediff = SystemClock.elapsedRealtime() - pausedat;

							// Need to set the paused time in the controller, so that it can send it down to the model
							_gpsController.set_pauseTime(timediff);
							_gpsController.set_baseTime(_gpsController.get_baseTime() + timediff);
							_gpsController.set_splitBaseTime(_gpsController.get_splitBaseTime() + timediff);
							_gpsController.startLocationCollection(true);
							_btnControl.setText("Stop");
						}
					}).setNegativeButton("Complete", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							_gpsController.completeWorkout();
							Class dvClass = null;
							Intent dvIntent;

							// Bring up
							try {
								dvClass = Class.forName("com.basalsolutions.gpsercise.SaveDataActivity");
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							dvIntent = new Intent(MappingActivity.this, dvClass);
							startActivity(dvIntent);
							// finish();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();

			// _GPSController.stopLocationCollection();
			Log.i("DATAVIEWACTIVITY", "User wants to stop. Setting listener to null");
		} else {
			// The user has clicked a "start" button
			// Set the text to stop and start the data collection
			_btnControl.setText("Stop");

			// Get data collecting!
			_gpsController.startLocationCollection(false);
		}

		FileLogger.writeToFile("MappingActivity", "----- MappingActivity onClick() -----");
	}

	@Override
	protected void onPause() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity onPause() +++++");
		FileLogger.writeToFile("MappingActivity", "_pointsDisplayed " + _pointsDisplayed);
		super.onPause();
		FileLogger.writeToFile("MappingActivity", "----- MappingActivity onPause() -----");
	}

	@Override
	protected void onRestart() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity onRestart() +++++");
		FileLogger.writeToFile("MappingActivity", "_pointsDisplayed " + _pointsDisplayed);
		super.onRestart();
		FileLogger.writeToFile("MappingActivity", "----- MappingActivity onRestart() -----");
	}

	@Override
	protected void onResume() {
		/*
		 * TODO:This is where the code should go to check for what data is currently displayed, if any
		 * 
		 * and then get the proper data that needs to be diplayed.
		 * 
		 * This is important when the app has gone into a state in which there is no data displayed on the map and the
		 * user brings the app back into the foreground
		 */
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity onResume() +++++");
		FileLogger.writeToFile("MappingActivity", "_pointsDisplayed " + _pointsDisplayed);
		super.onResume();

		// 7/27/2014:
		// If the _pointsDisplayed is 0, then we have to assume this is a resumption of a workout and that the activity
		// was killed off
		// if (_pointsDisplayed == 0) {
		// resume();
		// }

		FileLogger.writeToFile("MappingActivity", "----- MappingActivity onResume() -----");
	}

	@Override
	protected void onStart() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity onStart() +++++");
		FileLogger.writeToFile("MappingActivity", "_pointsDisplayed " + _pointsDisplayed);
		super.onStart();

		// TODO : This is where we need to recover from a "stopped" status and load everything back up into the display

		FileLogger.writeToFile("MappingActivity", "----- MappingActivity onStart() -----");
	}

	@Override
	protected void onStop() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity onStop() +++++");
		FileLogger.writeToFile("MappingActivity", "_pointsDisplayed " + _pointsDisplayed);
		super.onStop();
		FileLogger.writeToFile("MappingActivity", "----- MappingActivity onStop() -----");
	}

	private boolean readStateFile() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity.readStateFile +++++");
		File Root = Environment.getExternalStorageDirectory();
        boolean ret = false;
        
		if (Root.canRead()) {
			File inputfile = new File(Root, "GPSerciseState.txt");

			FileInputStream fis = null;

			try {
				fis = openFileInput(inputfile.getName());
				
				if (fis != null) {
					InputStreamReader isr = new InputStreamReader(fis);
					
					BufferedReader buff = new BufferedReader(isr);
					
					try {
						String dat = buff.readLine();
						
						if (dat.compareToIgnoreCase("1") == 0) {
							ret = true;
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
				}
			} catch (FileNotFoundException e) {
				// If the file is not found, then we don't need to do anything since ret will be false
			}

		}
		FileLogger.writeToFile("MappingActivity", "----- MappingActivity.readStateFile -----");
		return ret;
	}

	private void writeStateFile() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity.writeStateFile +++++");
		File Root = Environment.getExternalStorageDirectory();
		if (Root.canWrite()) {
			// Before this gets destroyed, we must write a file out to let the app know we were in the middle of a
			// workout
			File LogFile = new File(Root, "GPSerciseState.txt");
			FileWriter LogWriter = null;

			try {
				LogWriter = new FileWriter(LogFile, false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			BufferedWriter buff = new BufferedWriter(LogWriter);

			try {
				buff.write("1");
				buff.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				buff.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		FileLogger.writeToFile("MappingActivity", "----- MappingActivity.writeStateFile -----");
	}

	@Override
	protected void onDestroy() {
		FileLogger.writeToFile("MappingActivity", "+++++ MappingActivity onDestroy() +++++");
		FileLogger.writeToFile("MappingActivity", "_pointsDisplayed " + _pointsDisplayed);

		// If we are currently collecting data and the system has decided to destory this activity, then we have to save
		// state
		if (_gpsController.is_collectingdata()) {
			writeStateFile();
		}
		super.onDestroy();
		FileLogger.writeToFile("MappingActivity", "----- MappingActivity onDestroy() -----");
	}
}
