package com.basalsolutions.gpsercise;

public interface TimeListener {
	public abstract void onBaseTimeChange();
	public abstract void onSplitBaseTimeChange();

}
