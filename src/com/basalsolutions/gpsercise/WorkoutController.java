package com.basalsolutions.gpsercise;

import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class WorkoutController {

	int _notiID;
	Workout _workout;
	Context _context;
	private boolean _checkForSplit = false;
	private double _checkSplitValue = 0.90d;
	private long _elapsedTime = 0;
	
	public Workout get_workout() {
		return _workout;
	}

	public long get_elapsedTime() {
		return _elapsedTime;
	}

	public void set_elapsedTime(long _elapsedTime) {
		this._elapsedTime = _elapsedTime;
	}


	private void set_splitBaseTime(long _splitBaseTime) {
		FileLogger.writeToFile("WorkoutController", "+++++ GPSController set_splitBaseTime() +++++\n");
		_workout.set_splitBaseTime(_splitBaseTime);

		sendBroadcast("split-basetime-update", "splitbasetime", null);
		// _timeListener.onSplitBaseTimeChange();
		FileLogger.writeToFile("WorkoutController", "----- GPSController set_splitBaseTime() -----\n");
	}

	public boolean is_checkForSplit() {
		return _checkForSplit;
	}

	public void set_checkForSplit(boolean _checkForSplit) {
		this._checkForSplit = _checkForSplit;
	}

	public double get_checkSplitValue() {
		return _checkSplitValue;
	}

	public void set_checkSplitValue(double _checkSplitValue) {
		this._checkSplitValue = _checkSplitValue;
	}
	
	public WorkoutController(Context context) {
		// Create the workout object
		_workout = new Workout();
	}
	
	public WorkoutController(Context context, int notiID) {
		this(context);
		_notiID = notiID;
	}
	
	/* BroadcastReceiver to receive the new workout information from the workoutcontroller */
	private BroadcastReceiver pauseTimeUpdateMessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// Returned in the intent is an arraylist of location infos
			Bundle extras = intent.getExtras();

			long newsplitbasetime = extras.getLong("pausedtime");

			FileLogger.writeToFile("WorkoutController", "New pausetime received");
		}
	};
	
	private void sendBroadcast(String evt, String extraFieldName, LocationInfo locinfo) {
		Intent intent = new Intent(evt);

		if (extraFieldName.equalsIgnoreCase("workout")) {
			Bundle bundle = new Bundle();
			bundle.putParcelable("workout", _workout);
			intent.putExtras(bundle);
		} else if (extraFieldName.equalsIgnoreCase("splitbasetime")){
			intent.putExtra("splitbasetime", _workout.get_splitBaseTime());
		} else if (extraFieldName.equalsIgnoreCase("location")) {
			intent.putExtra("location", locinfo);
		}
		
		LocalBroadcastManager.getInstance(_context).sendBroadcast(intent);
	}

	// Determine if the current point gives us a split. A split is simply the whole mile (i.e 1.0, 2.0, 3.0, etc ...)
	private boolean isSplit(double dist) {
		long iPart = (long) dist;
		double fPart = dist - iPart;

		boolean ret = false;
		
		if (!_checkForSplit) {
			if (fPart >= this.get_checkSplitValue()) {
				this.set_checkForSplit(true);
			}

		} else {
			if ((fPart >= 0.00) && (fPart < this.get_checkSplitValue())) {
				ret = true;
			}
		}

		return ret;
	}

	// This is just for the initial location. We want to return the location to the map activity, but not count it towards the workout
	public void dontAddLocation(LocationInfo location) {
		// Just a send broadcast in this case
		sendBroadcast("one-shot-location", "location", location);
	}
	
	public void addLocation(LocationInfo location) {
		FileLogger.writeToFile("WorkoutController", "+++++ WorkoutController.addLocaton +++++");
		_workout.addLocation(location);

		// Get the information from the previous point if this is not the first point
		if (_workout.get_numLocations() == 1) {
			FileLogger.writeToFile("WorkoutController", "WorkoutController.addLocaton : First Location of the workout");
			_workout.set_startTime(new Date().getTime());
		} else {
			Date dt = new Date();
			
			Location prevloc = _workout.getLocation(_workout.get_numLocations() - 2);

			FileLogger.writeToFile("WorkoutController", "WorkoutController.addLocaton : prevloc = " + prevloc.getLatitude() + "/" + prevloc.getLongitude());
			
			double dist = _workout.get_distance() + prevloc.distanceTo(_workout.getLastLocation());

			_workout.set_distance(dist);

			FileLogger.writeToFile("WorkoutController", "WorkoutController.addLocaton : dist = " + dist);

			// Deal with the elapsed time since the last data point was received
			_elapsedTime = dt.getTime() - _workout.get_startTime();

			// Calculate the average speed from the start of the
			// First get the actual elapsed time by subtracting off any paused time
			long realelapsedinseconds = (_elapsedTime - _workout.get_pausedTime()) / 1000;

			// Calculate the speed in meters per second
			double mpers = dist / realelapsedinseconds;

			// Now get the miles per hour
			double avgspeed = GPSData.meterSpeedToMilesSpeed(mpers);
			
			// Need to pass a value to isSplit that is NOT meters. In this case, we'll use miles.
			double dist2 = GPSData.metersToMiles(dist);
			
			if (isSplit(dist2)) {
				// New split
				this.set_checkForSplit(false);

				LocationInfo locinfo = _workout.getLastLocation();

				locinfo.set_isSplit(true);
				locinfo.set_elapsedTime(_elapsedTime);
				
				// Still use meters here as all distances stored in the model shall be in meters
				locinfo.set_accumulatedDistance(dist);

				// Set the split timer
				long millis = SystemClock.elapsedRealtime();
				set_splitBaseTime(millis);
			}
	
			_workout.set_averageSpeed(avgspeed);
		}
		
		// 7/27/2014 - Each time a new point is added, we need to send it to the listeners
	    sendBroadcast("new-workout-data", "workout", null);
		FileLogger.writeToFile("WorkoutController", "----- WorkoutController.addLocaton -----");
	}

	public void broadcastWorkout() {
		FileLogger.writeToFile("WorkoutController", "+++++ WorkoutController.broadcastWorkout +++++");
	    sendBroadcast("new-workout-data", "workout", null);		
		FileLogger.writeToFile("WorkoutController", "----- WorkoutController.broadcastWorkout -----");
	}
}
