package com.basalsolutions.gpsercise;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.location.Address;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class WorkoutsAdapter extends ArrayAdapter<Workout> {

	private LayoutInflater _inflater;
	private List<Workout> _workouts;
	private GPSController _gpsController;
	private Context _context;
	private int _layoutResourceID;
	private AppPreferences _appPrefs;

	public WorkoutsAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
	}

	public WorkoutsAdapter(Context context, int resource, int textViewResourceId) {
		super(context, resource, textViewResourceId);
		// TODO Auto-generated constructor stub
	}

	public WorkoutsAdapter(Context context, int textViewResourceId, Workout[] objects) {
		super(context, textViewResourceId, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		WorkoutHolder holder = null;

		if (convertView == null) {
			holder = new WorkoutHolder();
			convertView = _inflater.inflate(_layoutResourceID, null);

			// Set up the holder with all the fields that are required to display
			holder._date = (TextView) convertView.findViewById(R.id.wrkDate);
			holder._distance = (TextView) convertView.findViewById(R.id.wrkDistance);
			holder._location = (TextView) convertView.findViewById(R.id.wrkLocation);
			holder._elapsedTime = (TextView) convertView.findViewById(R.id.wrkElapsedTime);

			convertView.setTag(holder);

		} else {
			holder = (WorkoutHolder) convertView.getTag();
		}

		SimpleDateFormat sdf = new SimpleDateFormat("E MMM d,yyyy");
		
		Workout wrk = _workouts.get(position);

		String date = sdf.format(new Date(wrk.get_startTime()));

		double distance = wrk.get_distance();
		String distancestr = null;
		
		Log.i("METERS","" + distance);
		
		// Display the distance in the proper format
		if (_appPrefs.get_measureFormat() == AppPreferences.MEASUREFORMAT_MI) {
			distance = GPSData.metersToMiles(distance);
			distancestr = String.format("%.2f", distance) + " " + AppPreferences.get_measureFormatString(AppPreferences.MEASUREFORMAT_MI);
		} else {
			distance = GPSData.metersToKM(distance);
			distancestr = String.format("%.2f", distance) + " " + AppPreferences.get_measureFormatString(AppPreferences.MEASUREFORMAT_KM);			
		}
		
		holder._distance.setText(distancestr);
		holder._date.setText(date);		

		Log.i("STARTTIME","" + wrk.get_startTime());
		Log.i("ENDTIME","" + wrk.get_endTime());
		Log.i("PAUSETIME","" + wrk.get_pausedTime());
		
		Long elapsedtimemillis = wrk.get_endTime() - wrk.get_startTime() + wrk.get_pausedTime();

		Log.i("MILLIS","" + elapsedtimemillis);
		
		String eltime = String.format("%02d:%02d:%02d", 
				TimeUnit.MILLISECONDS.toHours(elapsedtimemillis),
				TimeUnit.MILLISECONDS.toMinutes(elapsedtimemillis) -  
				TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(elapsedtimemillis)), // The change is in this line
				TimeUnit.MILLISECONDS.toSeconds(elapsedtimemillis) - 
				TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsedtimemillis)));   
		
		Log.i("HOURSMINSECS", eltime);
		
		holder._elapsedTime.setText(eltime);

		Address addr;
		try {
			addr = _gpsController.getAddressFromLoc(wrk.getLocation(0));
			holder._location.setText(addr.getLocality() + " " + addr.getAdminArea() + " " + addr.getCountryCode());
		} catch (IOException e) {
			holder._location.setText("Unknown Location");
		}
		return convertView;
	}

	public WorkoutsAdapter(Context context, int layoutResourceId, List<Workout> objects) {
		super(context, layoutResourceId, objects);
		_appPrefs = new AppPreferences(context);
		_workouts = objects;
		_context = context;
		_layoutResourceID = layoutResourceId;
		_gpsController = GPSController.getInstance(_context);
		_inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public WorkoutsAdapter(Context context, int resource, int textViewResourceId, Workout[] objects) {
		super(context, resource, textViewResourceId, objects);
	}

	public WorkoutsAdapter(Context context, int resource, int textViewResourceId, List<Workout> objects) {
		super(context, resource, textViewResourceId, objects);
	}

	static class WorkoutHolder {
		TextView _date;
		TextView _location;
		TextView _distance;
		TextView _elapsedTime;
	}
}
