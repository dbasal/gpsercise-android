package com.basalsolutions.gpsercise;

import android.location.Address;
import android.location.Location;

public class LocationInfo extends Location {

	private boolean _isSplit;
	private long _elapsedTime;
	private double _accumulatedDistance;
	
	public LocationInfo(String provider) {
		super(provider);
	}

	public LocationInfo(Location l) {
		super(l);
	}

	public boolean is_isSplit() {
		return _isSplit;
	}

	public void set_isSplit(boolean _isSplit) {
		this._isSplit = _isSplit;
	}

	public long get_elapsedTime() {
		return _elapsedTime;
	}

	public void set_elapsedTime(long _elapsedTime) {
		this._elapsedTime = _elapsedTime;
	}

	public double get_accumulatedDistance() {
		return _accumulatedDistance;
	}

	public void set_accumulatedDistance(double _accumulatedDistance) {
		this._accumulatedDistance = _accumulatedDistance;
	}

}
