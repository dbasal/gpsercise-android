package com.basalsolutions.gpsercise;

import java.io.IOException;

import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onDestroy() {
		FileLogger.writeToFile("MainActivity", "----- MainActivity onDestroy() -----");

		GPSController gc = GPSController.getInstance(this);
		gc.unbindGPSService();
		FileLogger.closeFile();
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		FileLogger.writeToFile("MainActivity", "----- MainActivity onStop() -----");
		// TODO Auto-generated method stub
		super.onStop();

		// Make sure to stop data collection
		// _GPSController.stopLocationCollection();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Create the log file
		try {
			FileLogger.CreateLogFile(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setContentView(R.layout.activity_main);

		// Setup the GPS
		// Check that the GPS is enabled. If not, then the user is gay
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (!gpsEnabled) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("GPS IS NOT ENABLED!").setCancelable(false)
					.setPositiveButton("I KNOW", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							finish();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();
		}

		// Setup the buttons to allow the user to continue in whichever
		// direction they want
		Button btnDataView = (Button) findViewById(R.id.btnDataView);
		Button btnSaveDataView = (Button) findViewById(R.id.btnSaveDataView);
		Button btnMapView = (Button) findViewById(R.id.btnMapView);
		Button btnWorkoutsView = (Button) findViewById(R.id.btnWorkoutsList);

		btnDataView.setOnClickListener(this);
		btnSaveDataView.setOnClickListener(this);
		btnMapView.setOnClickListener(this);
		btnWorkoutsView.setOnClickListener(this);

		// _gpsController.startGPSService();
		// _gpsController.bindGPSService();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {

		Class dvClass = null;
		Intent dvIntent = null;

		switch (v.getId()) {
			case R.id.btnDataView:
				try {
					dvClass = Class.forName("com.basalsolutions.gpsercise.DataViewActivity");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dvIntent = new Intent(MainActivity.this, dvClass);
				startActivity(dvIntent);
				// finish();
				break;
			case R.id.btnSaveDataView:
				try {
					dvClass = Class.forName("com.basalsolutions.gpsercise.SaveDataActivity");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dvIntent = new Intent(MainActivity.this, dvClass);
				startActivity(dvIntent);
				// finish();
				break;

			case R.id.btnMapView:
				try {
					dvClass = Class.forName("com.basalsolutions.gpsercise.MappingActivity");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dvIntent = new Intent(MainActivity.this, dvClass);
				startActivity(dvIntent);
				// finish();
				break;

			case R.id.btnWorkoutsList:
				Log.i("MainActivity", "btnWorkoutsList");

				try {
					dvClass = Class.forName("com.basalsolutions.gpsercise.WorkoutsActivity");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dvIntent = new Intent(MainActivity.this, dvClass);
				startActivity(dvIntent);
				// finish();
				break;
		}
	}

	@Override
	protected void onPause() {
		FileLogger.writeToFile("MainActivity","----- MainActivity onPause() -----");
		super.onPause();
	}

	@Override
	protected void onRestart() {
		FileLogger.writeToFile("MainActivity","----- MainActivity onRestart() -----");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		FileLogger.writeToFile("MainActivity","----- MainActivity onResume() -----");
		super.onResume();
	}
}
