package com.basalsolutions.gpsercise;

import java.util.ArrayList;
import java.util.Date;

import android.location.Location;
import android.os.SystemClock;
import android.util.Log;

public class GPSData {
	private double _distance = 0;
	private double _averageSpeed = 0;
	private long _elapsedTime = 0;
	private long _pauseTime = 0;
	private long _startTime = 0;
	private int _measureType = 2;
	private static final double MILES_IN_METERS = 0.000621371;
	private static final double METERS_PER_SEC_TO_MILES_PER_HOUR = 2.23694;
	private Workout _workout;
	private ArrayList<Location> _locList = new ArrayList<Location>();

	public GPSData() {
	}

	public static double metersToMiles(double mtrs) {		
		return mtrs * MILES_IN_METERS;
	}

	public static double metersToKM(double mtrs) {		
		return mtrs / 1000;
	}

	public static double meterSpeedToMilesSpeed(double meterspersecond) {
		return meterspersecond * METERS_PER_SEC_TO_MILES_PER_HOUR;
	}
}
