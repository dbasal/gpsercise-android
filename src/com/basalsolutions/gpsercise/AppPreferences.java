package com.basalsolutions.gpsercise;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPreferences {
	public static final int MEASUREFORMAT_KM = 1;
	public static final int MEASUREFORMAT_MI = 2; // Default

	private String _measureFormatString = "mi";
	private int _measureFormat = MEASUREFORMAT_MI;
	private SharedPreferences _sharedPrefs;
	private String _paceString = "mph";

	public AppPreferences(Context context) {
		// Setup the pointer to shared preferences
		_sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);

		// On creation, get all the settings
		getPreferences();
	}

	public void savePreferences() {
	}

	private void getPreferences() {
		set_measureFormat(_sharedPrefs.getInt("Measure Format", 2));
	}

	public int get_measureFormat() {
		return _measureFormat;
	}

	public void set_measureFormat(int measureFormat) {
		this._measureFormat = measureFormat;

		switch (measureFormat) {
		case 1:
			_measureFormatString = "km";
		case 2:
			_measureFormatString = "mi";
		}
	}

	public static String get_measureFormatString(int measureFormat) {
		String retVal = "";
		
		switch (measureFormat) {
		case 1:
			retVal = "km";
		case 2:
			retVal =  "mi";
		}
		return retVal;
	}

	public static String get_paceString(int measureFormat) {
		String retVal = "";
		
		switch (measureFormat) {
		case 1:
			retVal = "kph";
		case 2:
			retVal =  "mph";
		}
		return retVal;
	}

	public void set_paceString(String _paceString) {
		this._paceString = _paceString;
	}
}
