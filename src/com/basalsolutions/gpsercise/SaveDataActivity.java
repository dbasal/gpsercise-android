package com.basalsolutions.gpsercise;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SaveDataActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_save_data);
						
		Button saveDataButton = (Button) findViewById(R.id.btnSaveData);
		saveDataButton.setOnClickListener(this);	

		Button btnCheckSQL = (Button) findViewById(R.id.btnGetInfo);
		btnCheckSQL.setOnClickListener(this);
		
		Button btnSaveToCloud = (Button) findViewById(R.id.btnSendToSimpleDB);
		btnSaveToCloud.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.save_data, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
FileLogger.writeToFile("SaveDataActivity","+++++ SaveDataActivity::onClick +++++");
		// TODO Auto-generated method stub
		DatabaseController dbController = new DatabaseController(this);
		switch(v.getId()) {
			case R.id.btnSaveData:
FileLogger.writeToFile("SaveDataActivity","SaveDataActivity : Save Data Button Pressed");
				dbController.getWritableDatabase();
			
				try {
FileLogger.writeToFile("SaveDataActivity","Calling dbController.addWorkout");
					dbController.addWorkout(GPSController.getInstance(this).getWorkout(),false);
FileLogger.writeToFile("SaveDataActivity","Return from dbController.addWorkout");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				break;
			case R.id.btnGetInfo:
				// Call db controller to get all the information we need
				TextView tvWorkouts = (TextView) findViewById(R.id.tvworkouts);
				TextView tvLocations = (TextView) findViewById(R.id.tvLocations);

				long wkcount = dbController.getWorkoutRecordsCount();
				tvWorkouts.setText("Workouts : " + wkcount);
				
				long loccount = dbController.getLocationRecordsCount();
				tvLocations.setText("Locations : " + loccount);
				
				break;
				
			case R.id.btnSendToSimpleDB:
				saveToCloud(dbController);
				break;
		}
FileLogger.writeToFile("SaveDataActivity","----- SaveDataActivity::onClick -----");
	}

	private void saveToCloud(DatabaseController dbController) {
		List<Long> wrkids = dbController.getWorkoutsToSync();
		
		AWSController awscontroller = new AWSController(this);
	
		awscontroller.saveToCloud();
		
		try {
		//	dbController.saveToCloud();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
