package com.basalsolutions.gpsercise;

import java.util.ArrayList;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.RemoteViews;

public class GPSService extends Service {
	public static final int MSG_NEW_LOCATION = 1;
	public static final int MSG_REGISTER_CLIENT = 2;
	public static final int MSG_COLLECTING_DATA_START = 3;
	public static final int MSG_COLLECTING_DATA_STOP = 4;
	public static final int MSG_COLLECTING_DATA_RESUME = 5;
	public static final int MSG_COLLECTING_ONE_SHOT = 7;
	public static final int MSG_BROADCAST_WORKOUT = 8;
	// public static final int MSG_UNREGISTER_CLIENT = 6;
	private static final int TEN_SECONDS = 1000 * 10;
	private static final int ONE_MINUTE = 1000 * 60;
	private static final int TWO_MINUTES = ONE_MINUTE * 2;
	private boolean _collectingdata = false;
	private static final int _notiID = 6969;
	private boolean _oneshot = false;

	// Added 8/6/2014 : The workout is now connected to the service through the WorkoutController class
	private WorkoutController _workoutController;

	// private GPSData _gpsData;
	private LocationListener _locationListener = new GPSLocationListener();
	// private ArrayList<Messenger> _clients = new ArrayList<Messenger>();
	private LocationManager _locationManager;
	private Location _lastLocation;
	private Messenger _messenger;

	// final Messenger _messenger = new Messenger(new IncomingHandler());

	public boolean is_collectingdata() {
		return _collectingdata;
	}

	public GPSService() {
		Log.i("GPSSERVICE", "CONSTRUCTOR");

		_workoutController = new WorkoutController(this);
	}

	private void resumeDataCollection() {
		// Make sure _gpsData is not null. If it is, then just call startDataCollection
		_locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TEN_SECONDS, 20, _locationListener);
	}

	private Notification createNotification() {
		Intent intent = new Intent(this, MappingActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		RemoteViews notiView = new RemoteViews(this.getPackageName(), R.layout.notification_view);

		Workout wrk = _workoutController.get_workout();

		notiView.setTextViewText(R.id.notitvDistance, Double.toString(GPSData.metersToMiles(wrk.get_distance())) + AppPreferences.get_measureFormatString(AppPreferences.MEASUREFORMAT_MI));
		// notiView.setTextViewText(R.id.notitvTime, wrk.)
		notiView.setTextViewText(R.id.notitvAvgSpeed, Double.toString(wrk.get_averageSpeed()) + AppPreferences.get_paceString(AppPreferences.MEASUREFORMAT_MI));

		// Using notiLayout here means that the user can click anywhere on the notification to bring up the activity
		notiView.setOnClickPendingIntent(R.id.notiLayout, pendingIntent);

		Notification noti = new Notification.Builder(this).setSmallIcon(R.drawable.stopwatchnoti).setContentTitle("GPSercise").setContent(notiView)
				.setContentIntent(pendingIntent).build();

		return noti;

		// NotificationManager nm = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);
		// nm.notify(69,noti);
	}

	private void startDataCollection() {
		FileLogger.writeToFile("GPSService", "+++++ GPSSERVICE startDataCollection +++++\n");

		if (!this._collectingdata) {
			_locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

			// Create the GPS and Network providers
			Location _gpsLocation = requestUpdatesFromProvider(LocationManager.GPS_PROVIDER, "GPS Provider is not enabled");

			// Location _networkLocation = requestUpdatesFromProvider(
			// LocationManager.NETWORK_PROVIDER,
			// "Network Provider is not enabled");

			Location _networkLocation = null;

			Location location = getBetterLocation(_gpsLocation, _networkLocation);

			// If this location is not null, add it to the list
			if (location != null) {
				createMessage(MSG_NEW_LOCATION, location);
			}

			this._collectingdata = true;

			// Build the notification to allow this service to run in the foreground
			Notification noti = createNotification();

			startForeground(_notiID, noti);
		}
		
		FileLogger.writeToFile("GPSService", "----- GPSSERVICE startDataCollection -----\n");
	}

	private void stopDataCollection() {
		FileLogger.writeToFile("GPSService", "+++++ GPSSERVICE stopDataCollection +++++\n");
		if (this.is_collectingdata()) {
			FileLogger.writeToFile("GPSService", "Turning off data collection listener");
			// Turn off the location requests
			_locationManager.removeUpdates(_locationListener);
			this._collectingdata = false;
		}

		FileLogger.writeToFile("GPSService", "----- GPSSERVICE stopDataCollection -----\n");
	}

	@Override
	public IBinder onBind(Intent intent) {
		return _messenger.getBinder();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Since this could be called by many activities, we need to see if it
		// is already collecting data.
		// It would be problematic to try to run things twice or more
		FileLogger.writeToFile("GPSService", "+++++GPSService : onStartCommand");

		Bundle bundle = intent.getExtras();
		_messenger = bundle.getParcelable("messenger");
		int collectmode = bundle.getInt("collectmode");
		boolean stop = false;

		try {
			Message msg = Message.obtain();

			_oneshot = false;

			switch (collectmode) {
				case 0:
					msg.what = MSG_COLLECTING_DATA_STOP;
					stopDataCollection();
					stop = true;
					break;
				case 1:
					msg.what = MSG_COLLECTING_ONE_SHOT;
					startDataCollection();
					_oneshot = true;
					break;
				case 2:
					msg.what = MSG_COLLECTING_DATA_START;
					startDataCollection();
					break;
				case 3:
					msg.what = MSG_BROADCAST_WORKOUT;
					_workoutController.broadcastWorkout();
					break;
			}

			_messenger.send(msg);
		} catch (RemoteException e) {

		}

		if (stop) {
			stopSelf();
		}

		return START_STICKY;
	}

	// Used as initial setup for location providers
	private Location requestUpdatesFromProvider(final String provider, final String errmsg) {
		FileLogger.writeToFile("GPSService", "GPSSERVICE : requestUpdatesFromProvider");
		Location location = null;
		if (_locationManager.isProviderEnabled(provider)) {
			_locationManager.requestLocationUpdates(provider, TEN_SECONDS, 20, _locationListener);
			// location = _locationManager.getLastKnownLocation(provider);
		} else {
			// Toast.makeText(_context, errmsg, Toast.LENGTH_LONG).show();
		}
		return location;
	}

	protected Location getBetterLocation(Location newLocation, Location currentBestLocation) {
		FileLogger.writeToFile("GPSService", "GPSSERVICE : getBetterLocation");
		if (currentBestLocation == null) {
			FileLogger.writeToFile("GPSService", "GPSSERVICE : currentBestLocation is null. Returning newLocation");
			// A new location is always better than no location
			return newLocation;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = newLocation.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved.
		if (isSignificantlyNewer) {
			return newLocation;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return currentBestLocation;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (newLocation.getAccuracy() - currentBestLocation.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(newLocation.getProvider(), currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return newLocation;
		} else if (isNewer && !isLessAccurate) {
			return newLocation;
		} else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
			return newLocation;
		}

		return currentBestLocation;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}

	private final class GPSLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			FileLogger.writeToFile("\nGPSService", "+++++ GPSSERVICE onLocationChanged +++++\n");
			Location retLoc = null;

			FileLogger.writeToFile("GPSService", "retLoc Lat = " + location.getLatitude());
			FileLogger.writeToFile("GPSService", "retLoc Lon = " + location.getLongitude());

			// Need to check with the GPSConverter if these coords need transformation (China)
			GPSConverter gpsConv = new GPSConverter();

			GPSPoint point = gpsConv.getEncryPoint(location.getLongitude(), location.getLatitude());

			// Check to see what was returned
			if (point == null) {
				FileLogger.writeToFile("GPSService", "point is null");
			} else {
				FileLogger.writeToFile("GPSService", "Original Lat/Lon = " + location.getLatitude() + "/" + location.getLongitude());
				FileLogger.writeToFile("GPSService", "New Lat/Lon = " + point.getY() + "/" + point.getX());

				location.setLatitude(point.getY());
				location.setLongitude(point.getX());
			}

			// If we don't have a current location,then this location becomes
			// the current location
			if (_lastLocation == null) {
				FileLogger.writeToFile("GPSService", "GPSSERVICE : lastLocation is null. Returning location");
				retLoc = location;
			} else {
				// If we have a previous location, then compare it against the
				// new one to see which is better
				retLoc = getBetterLocation(location, _lastLocation);
			}

			// Do not create the notification is this is just the "one shot" location check
			if (!_oneshot) {
				// 8/6/2014 : GPSService now connects to the workout through the workoutcontroller
				// 8/13/2014 : Add it to the workout for now. Not sure this is the best idea, but assuming the user is
				// starting the app
				// up in order to start a workout from their current position
				_workoutController.addLocation(new LocationInfo(retLoc));

				// Now we need to update the notification to keep it in sync
				createNotification();
			} else {
				stopDataCollection();
				_workoutController.dontAddLocation(new LocationInfo(retLoc));
			}

			// 8/6/2014 : Removed the createMessage call since we do this in the workout object now.
			// createMessage(MSG_NEW_LOCATION, null);
			FileLogger.writeToFile("GPSService", "----- GPSSERVICE onLocationChanged -----\n");
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	public void createMessage(int messagetype, Location loc) {
		try {
			Message msg = Message.obtain();
			msg.what = messagetype;

			// Fill out the rest of the message based on the messagetype
			switch (messagetype) {
				case MSG_NEW_LOCATION:
					FileLogger.writeToFile("GPSService", "GPSSERVICE : Creating New Location Message");
					// msg.obj = loc;
					// 6/21/2014 : Now returning the entire workout to allow the view to choose which items to draw
					// msg.obj = _workout;
					break;
			}

			_messenger.send(msg);
		} catch (RemoteException e) {
			// If we get here, the client is dead, and we should remove it
			// from the list
			// log.debug("Removing client: " + clients.get(i));
			// _clients.remove(i);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
