package com.basalsolutions.gpsercise;

import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

public class DataViewActivity extends Activity implements GPSDataListener, OnClickListener, TimeListener {

	private TextView _tvLat;
	private TextView _tvLon;
	private TextView _tvDist;
	private TextView _tvSpeed;
	private TextView _tvAvgSpeed;
	private TextView _tvCurrentSpeed;
	private Chronometer _crOverallTime;
	private Chronometer _crSplitTime;
	private boolean _hasDataBeenRead = false;
	private AppPreferences _appPrefs;
	private Button _btnControl;
	private GPSController _gpsController;
	private long _pausedat = 0;
	private TextView _tvPaused;
	private TextView _tvResume;
	private TextView _tvTimeDiff;
	private TextView _tvSplit;
	private TextView _tvSplitDetected;
	private TextView _tvDebug;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FileLogger.writeToFile("DataViewActivity","----- DataViewActivity onCreate() -----");
		setContentView(R.layout.activity_data_view);

		// TextViews
		_tvLat = (TextView) findViewById(R.id.txtLat);
		_tvLon = (TextView) findViewById(R.id.txtLon);
		_tvDist = (TextView) findViewById(R.id.txtDistance);
		_tvSpeed = (TextView) findViewById(R.id.tvSpeed);
		_tvAvgSpeed = (TextView) findViewById(R.id.tvAverageSpeed);
		_tvCurrentSpeed = (TextView) findViewById(R.id.tvCurrentSpeed);
		// End TextViews

		// Chronometers
		_crOverallTime = (Chronometer) findViewById(R.id.crOverallTime);
		_crSplitTime = (Chronometer) findViewById(R.id.crSplitTime);
		// End Chronometers

		// Debug Stuff
		_tvPaused = (TextView) findViewById(R.id.tvPaused);
		_tvResume = (TextView) findViewById(R.id.tvResumed);
		_tvTimeDiff = (TextView) findViewById(R.id.tvTimeDiff);
		_tvSplit = (TextView) findViewById(R.id.tvSplit);
		_tvSplitDetected = (TextView) findViewById(R.id.tvSplitDetected);
		_tvDebug = (TextView) findViewById(R.id.tvDebug);
		// End Debug Stuff

		setup();
	}

	private void updateUI(LocationInfo locinfo) {
		FileLogger.writeToFile("DataViewActivity","----- DataViewActivity updateUI() -----");

		double dist = locinfo.get_accumulatedDistance();
		
		_tvDist.setText(String.format("%.2f", dist) + " " + AppPreferences.get_measureFormatString(AppPreferences.MEASUREFORMAT_MI));
	//	double avgspeed = _gpsController.get_averageSpeed();

		_tvLat.setText("Lat : " + locinfo.getLatitude());
		_tvLon.setText("Lon : " + locinfo.getLongitude());

		_tvDist.setText(String.format("%.2f", dist) + " " + AppPreferences.get_measureFormatString(AppPreferences.MEASUREFORMAT_MI));
		_tvSpeed.setText("Speed : " + locinfo.getSpeed());
	//	_tvAvgSpeed.setText("Avg: " + String.format("%.2f", avgspeed) + AppPreferences.get_paceString(AppPreferences.MEASUREFORMAT_MI));

		double spd = GPSData.meterSpeedToMilesSpeed(locinfo.getSpeed());

		_tvCurrentSpeed.setText("Current: " + String.format("%.2f", spd) + AppPreferences.get_paceString(AppPreferences.MEASUREFORMAT_MI));

/*		if (_gpsController.is_firstLocation()) {
			// Start the chronos
			_crOverallTime.start();
			_crSplitTime.start();
		} */
	}

	private void resume() {
		FileLogger.writeToFile("DataViewActivity","----- DataViewActivity resume() -----");
		// First get all the information from the GPSController to set the state
		// properly
		//updateUI();

		// Need to start up the chronos again and make sure the control button
		// reads Stop
		_btnControl.setText("Stop");

		// Set the base time of the chronos to the base times from the
		// controller
		_crOverallTime.setBase(_gpsController.get_baseTime());
		_crSplitTime.setBase(_gpsController.get_splitBaseTime());
		_crOverallTime.start();
		_crSplitTime.start();
	}

	private void setup() {
		FileLogger.writeToFile("DataViewActivity","----- DataViewActivity setup() -----");
		_btnControl = (Button) findViewById(R.id.btnControl);
		_btnControl.setOnClickListener(this);

		_gpsController = GPSController.getInstance(this);
		_appPrefs = new AppPreferences(this);

/*		if (!_gpsController.is_collectingdata()) {
			_gpsController.startGPSService(2);
		} else {
			_hasDataBeenRead = true;
			resume();
		}
*/
		_gpsController.set_timeListener(this);
		_gpsController.set_gpsDataListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.data_view, menu);
		return true;
	}

	private void initializeControls() {
		FileLogger.writeToFile("DataViewActivity","----- DataViewActivity initializeControls() -----");

		long millis = SystemClock.elapsedRealtime();
		_gpsController.set_baseTime(millis);
		_gpsController.set_splitBaseTime(millis);
	}

	@Override
	public void onGPSData(Workout workour) {
		// Check to see if this is the first location data received
		if (!_hasDataBeenRead) {
			// This is the first reading;
			// Need to setup all baseline data
			initializeControls();

			_hasDataBeenRead = true;
		}

	//	updateUI(locinfo);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (_btnControl.getText().toString().equalsIgnoreCase("Stop")) {
			FileLogger.writeToFile("DataViewActivity","********* USER HIT STOP BUTTON *********");
			// We have a user that has clicked a "stop" button
			// Change the text back to start
			_btnControl.setText("Start");

			_pausedat = SystemClock.elapsedRealtime();
			_tvPaused.setText("Pause Time: " + _pausedat);
			_gpsController.stopLocationCollection();
			_crOverallTime.stop();
			_crSplitTime.stop();

			// Need to check with the user if they wish to pause or stop
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Workout stopped. Please select option").setCancelable(false)
					.setPositiveButton("Resume", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// Just let the place know we are pausing,
							// but will continue
							// When the user clicks this button, we must
							// change the base time for the chronos to
							// make sure they are keeping track properly
							FileLogger.writeToFile("DataViewActivity","********* USER SELECTED TO RESUME *********");
							long timediff = SystemClock.elapsedRealtime() - _pausedat;

							// Need to set the paused time in the controller, so that it can send it down to the model
							_gpsController.set_pauseTime(timediff);
							_tvTimeDiff.setText("Time Diff = " + timediff);
							_gpsController.set_baseTime(_gpsController.get_baseTime() + timediff);
							_gpsController.set_splitBaseTime(_gpsController.get_splitBaseTime() + timediff);
							_tvResume.setText("New Base Time = " + _gpsController.get_baseTime());
							_gpsController.startLocationCollection(true);
							_btnControl.setText("Stop");
						}
					}).setNegativeButton("Complete", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							_gpsController.completeWorkout();
							Class dvClass = null;
							Intent dvIntent;

							// Bring up
							try {
								dvClass = Class.forName("com.basalsolutions.gpsercise.SaveDataActivity");
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							dvIntent = new Intent(DataViewActivity.this, dvClass);
							startActivity(dvIntent);
							// finish();
						}
					});
			AlertDialog alert = builder.create();
			alert.show();

			// _GPSController.stopLocationCollection();
		} else {
			// The user has clicked a "start" button
			// Set the text to stop and start the data collection
			_btnControl.setText("Stop");

			// Need to check if GPS data is already flowing
			if (_gpsController.is_serviceBound()) {
				_gpsController.startLocationCollection(false);
			}
		}

	}

	@Override
	protected void onDestroy() {
		FileLogger.writeToFile("DataViewActivity","----- DataViewActivity onDestroy() -----");

		// _gpsController.unbindGPSService();

		super.onDestroy();
	}

	@Override
	public void onBaseTimeChange() {
		FileLogger.writeToFile("DataViewActivity","----- DataViewActivity onBaseTimeChange() -----");

		_crOverallTime.stop();
		_crOverallTime.setBase(_gpsController.get_baseTime());
		_crOverallTime.start();
	}

	@Override
	public void onSplitBaseTimeChange() {
		_crSplitTime.stop();
		_crSplitTime.setBase(_gpsController.get_splitBaseTime());
		_crSplitTime.start();
	}

}
