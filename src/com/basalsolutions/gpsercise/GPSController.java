package com.basalsolutions.gpsercise;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;

public class GPSController {
	private static final double MILES_IN_METERS = 0.000621371;
	private boolean _collectingdata = false;
	private Context _context;
	private Messenger _gpsMessenger;
	private boolean _serviceBound = false;
	private Intent _serviceIntent;
	private long _baseTime = 0;
	private long _splitBaseTime = 0;
	private double _distance = 0;
	private boolean _checkForSplit = false;
	private boolean _splitDetected = false;
	private TimeListener _timeListener;
	private Workout _workout;
	private GPSDataListener _gpsDataListener;
	private static volatile GPSController instance;
	private int _workoutindex = 0;
	Messenger _messenger = new Messenger(new IncomingHandler());

	private void sendPauseTimeBroadcast(String evt, long pausetime) {
		Intent intent = new Intent(evt);
		intent.putExtra("pausetime", pausetime);
		
		LocalBroadcastManager.getInstance(_context).sendBroadcast(intent);
	}
	
	public boolean is_checkForSplit() {
		return _checkForSplit;
	}

	public void set_checkForSplit(boolean _checkForSplit) {
		this._checkForSplit = _checkForSplit;
	}

	public void set_pauseTime(long pauseTime) {
		sendPauseTimeBroadcast("pausetime-update-to-workout", pauseTime);
	}

	public long get_baseTime() {
		return _baseTime;
	}

	public void set_timeListener(TimeListener _timeListener) {
		this._timeListener = _timeListener;
	}

	public void set_baseTime(long _baseTime) {
		FileLogger.writeToFile("GPSController", "+++++ GPSCONTROLLER set_baseTime() +++++\n");

		this._baseTime = _baseTime;

		_timeListener.onBaseTimeChange();
		FileLogger.writeToFile("GPSController", "----- GPSCONTROLLER set_baseTime() -----\n");
	}

	public boolean is_serviceBound() {
		return _serviceBound;
	}

	public void unbindGPSService() {
		FileLogger.writeToFile("GPSController", "+++++ GPSController unbindGPSService() +++++\n");

		// Only unbind if service is bound
		if (_serviceBound) {
			this._context.unbindService(gpsServiceConnection);

			// Once we unbind, we also have to unregister
			/*
			 * Message msg = Message.obtain(null, GPSService.MSG_UNREGISTER_CLIENT); try { _gpsMessenger.send(msg); }
			 * catch (RemoteException e) { e.printStackTrace(); }
			 */
		}
		FileLogger.writeToFile("GPSController", "----- GPSController unbindGPSService() -----\n");
	}

	public void set_gpsDataListener(GPSDataListener _gpsDataListener) {
		this._gpsDataListener = _gpsDataListener;
	}

	public static GPSController getInstance(Context context) {
		FileLogger.writeToFile("GPSController", "+++++ GPSController getInstance() +++++\n");
		if (instance == null) {
			synchronized (GPSController.class) {
				if (instance == null) {
					instance = new GPSController(context);
				}
			}
		}

		FileLogger.writeToFile("GPSController", "----- GPSController getInstance() -----\n");
		return instance;
	}

	/* BroadcastReceiver to receive the new split base time from the workoutcontroller */
	private BroadcastReceiver splitBaseTimeUpdateReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// Returned in the intent is an arraylist of location infos
			Bundle extras = intent.getExtras();

			@SuppressWarnings("unchecked")
			long newsplitbasetime = extras.getLong("splitbasetime");

			FileLogger.writeToFile("GPSController", "New SplitBaseTime received");

			set_splitBaseTime(newsplitbasetime);
		}
	};
	
	protected Address getAddressFromLoc(Location loc) throws IOException {
		// Get the address information for this location
		Geocoder gc = new Geocoder(_context);
		Address addr = null;

		List<Address> addrlist = new ArrayList<Address>();

		addrlist = gc.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);

		if (addrlist != null) {
			addr = addrlist.get(0);
		}

		return addr;
	}

	// IncomingHandler to handle messages sent to us by the GPSService. This
	// will be location updates
	// 8/6/2014 : This is no longer used for the return of workout data. That is now done via a Broadcast message from
	// the workoutController
	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			FileLogger.writeToFile("GPSController", "+++++ GPSCONTROLLER IncomingHandler::handleMessage +++++\n");
			switch (msg.what) {
				case GPSService.MSG_NEW_LOCATION:
					FileLogger.writeToFile("GPSController", "MSG_NEW_LOCATION RECEIVED");
					// This is where we need to grab the GPSData instance
					// 6/21/2014 : The GPSService now returns the entire workout
					// Workout workout = (Workout) msg.obj;
					// Location loc = (Location) msg.obj;

					// processGPSData(loc);

					// Increment the workout index so that we will know where we are next time
					_workoutindex += 1;
					break;
				case GPSService.MSG_COLLECTING_DATA_START:
					FileLogger.writeToFile("GPSController", "MSG_COLLECTING_DATA_START RECEIVED");
					_collectingdata = true;
					break;
				case GPSService.MSG_COLLECTING_DATA_STOP:
					_collectingdata = false;
					break;
				case GPSService.MSG_COLLECTING_ONE_SHOT:
					FileLogger.writeToFile("GPSController", "MSG_COLLECTING_ONE_SHOT RECEIVED");
					_collectingdata = true;
					break;
				default:
					super.handleMessage(msg);
			}
			FileLogger.writeToFile("GPSController", "----- GPSCONTROLLER IncomingHandler::handleMessage -----\n");
		}
	}

	// Make the connection to the service
	private ServiceConnection gpsServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			FileLogger.writeToFile("GPSController", "+++++ GPSCONTROLLER onServiceConnected +++++");
			_gpsMessenger = new Messenger(service);
			try {
				Message msg = Message.obtain(null, GPSService.MSG_REGISTER_CLIENT);
				msg.replyTo = _messenger;
				FileLogger.writeToFile("GPSController", "Sending MSG_REGISTER_CLIENT to service");
				_gpsMessenger.send(msg);
				// log.debug("Connected to service");
				_serviceBound = true;

			} catch (RemoteException e) {
				// Here, the service has crashed even before we were able to
				// connect
			}
			FileLogger.writeToFile("GPSController", "----- GPSCONTROLLER onServiceConnected -----");
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			FileLogger.writeToFile("GPSCONTROLLER", "Service Disconnected");
			_serviceBound = false;
		}
	};

	public boolean is_collectingdata() {
		return _collectingdata;
	}

	public void setGPSDataListener(GPSDataListener gpsListener) {
		this._gpsDataListener = gpsListener;
	}

	public void bindGPSService() {
		FileLogger.writeToFile("GPSController", "+++++ GPSController bindGPS() +++++\n");
		this._context.bindService(_serviceIntent, gpsServiceConnection, Context.BIND_AUTO_CREATE);
		FileLogger.writeToFile("GPSController", "----- GPSController bindGPS() -----\n");
	}

	private void startGPSService(int collectionmode) {
		FileLogger.writeToFile("GPSController", "+++++ GPSController startGPSService() +++++\n");

		_serviceIntent.removeExtra("collectmode");
		_serviceIntent.putExtra("collectmode", collectionmode);

		ComponentName cn = this._context.startService(_serviceIntent);

		if (cn == null) {
			FileLogger.writeToFile("GPSController", "GPSCONTROLLER::startGPSService : null returned");
		}

		FileLogger.writeToFile("GPSController", "----- GPSController startGPSService() -----\n");
	}

	public void startLocationCollection(boolean resume) {
		FileLogger.writeToFile("GPSController", "+++++ GPSController startLocationCollection() +++++\n");

		/*
		 * if (_workout == null) { _workout = new Workout(_context); }
		 */
		startGPSService(2);

		_collectingdata = true;
		FileLogger.writeToFile("GPSController", "----- GPSController startLocationCollection() -----\n");
	}

	// completeWorkout is called when the user has decided that their workout is completely finished.
	public void completeWorkout() {
		_collectingdata = false;
		
		// Sending a 0 into the startService command will actually shutdown the service
		startGPSService(0);
		
//		_workout.set_distance(_distance);
	//	_workout.set_endTime(_workout.getLastLocation().getTime());
	}

	public Workout getWorkout() {
		return _workout;
	}

	public void stopLocationCollection() {
		FileLogger.writeToFile("GPSController", "+++++ GPSController stopLocationCollection() +++++\n");

		_serviceIntent.putExtra("collectmode", 0);

		startGPSService(0);

		_collectingdata = false;
		FileLogger.writeToFile("GPSController", "----- GPSController stopLocationCollection() -----\n");
	}

	public GPSController(Context context) {
		FileLogger.writeToFile("GPSController", "+++++ GPSController Constructor +++++");

		_context = context;
		// Class gpsServiceClass = null;
		//
		// try {
		// gpsServiceClass =
		// Class.forName("com.basalsolutions.gpsercise.GPSService");
		// } catch (ClassNotFoundException e) {
		// e.printStackTrace();
		// }

		_serviceIntent = new Intent(_context, GPSService.class);

		// 2-22-2014 : Need to add a messenger object to allow the service to communicate back to the controller
		Messenger msg = new Messenger(new IncomingHandler());

		_serviceIntent.putExtra("messenger", msg);

		LocalBroadcastManager.getInstance(context).registerReceiver(splitBaseTimeUpdateReceiver, new IntentFilter("update-split-basetime"));

		// Upon initial instantiation, issue a one shot location request
		startGPSService(1);
		
		// The messenger needs to be placed in the intent extras
		FileLogger.writeToFile("GPSController", "----- GPSController Constructor -----");
	}

	public LocationInfo get_lastLocation() {
		return _workout.getLastLocation();
	}

	public double get_distance(int type) {

		// DO NOT mess with the variable accumulating the distance in meters
		double distance = _distance;

		switch (type) {
			case 1:
				distance /= 1000;
				break;
			case 2:
				distance *= MILES_IN_METERS;
				break;
		}

		return distance;
	}

	public long get_splitBaseTime() {
		return _splitBaseTime;
	}

	public void set_splitBaseTime(long _splitBaseTime) {
		FileLogger.writeToFile("GPSController", "+++++ GPSController set_splitBaseTime() +++++\n");
		this._splitBaseTime = _splitBaseTime;

		_timeListener.onSplitBaseTimeChange();
		FileLogger.writeToFile("GPSController", "----- GPSController set_splitBaseTime() -----\n");
	}

	public boolean is_splitDetected() {
		return _splitDetected;
	}

	public void set_splitDetected(boolean _splitDetected) {
		this._splitDetected = _splitDetected;
	}

	public ArrayList<LocationInfo> getLocations() {

		if (_workout == null) {
			return null;
		}

		return _workout.getLocations();
	}

	public void broadcastWorkout() {
		startGPSService(4);
	}
}
