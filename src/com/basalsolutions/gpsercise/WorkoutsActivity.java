package com.basalsolutions.gpsercise;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;

public class WorkoutsActivity extends Activity {

	private ListView _listview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workouts);
		Log.i("WORKOUTSACTIVITY", "onCreate");
		
		_listview = (ListView) findViewById(R.id.lvWorkouts);
		
		// Get the items to load into the listview
		DatabaseController dbController = new DatabaseController(this);
		List<Workout> workouts = dbController.getWorkoutData();
		
		Log.i("WORKOUTSACTIVITY","Number of workouts : " + workouts.size());
		
		WorkoutsAdapter adapter = new WorkoutsAdapter(this, R.layout.workout_list_item, workouts);
		_listview.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.workouts, menu);

		return true;
	}
}
