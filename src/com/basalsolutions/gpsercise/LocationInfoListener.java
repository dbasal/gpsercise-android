package com.basalsolutions.gpsercise;

import android.location.Location;

public interface LocationInfoListener {
	public abstract void onLocationData();
}
