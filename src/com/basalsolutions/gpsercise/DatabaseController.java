package com.basalsolutions.gpsercise;

import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.util.Log;

public class DatabaseController extends SQLiteOpenHelper {

	private SQLiteDatabase _database;
	private Context _context;
	private static final String _dbName = "GPSexercise";
	private static final String _locationsTableName = "Locations";
	private static final String _workoutsTableName = "Workouts";
	private static final String _splitsTableName = "Splits";
	private static final int _dbVersion = 5;
	private String[] _locationProjection = { "ID", "ID_WORKOUT", "LAT", "LON", "TIME", "ACCURACY", "BEARING", "ALTITUDE", "SPLIT", "ELAPSEDTIME",
			"ACCUMDIST" };
	private String[] _workoutProjection = { "ID", "ACTIVE", "STARTTIME", "ENDTIME", "PAUSEDTIME", "DISTANCE" };

	public DatabaseController(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@SuppressLint("NewApi")
	public DatabaseController(Context context, String name, CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}

	public DatabaseController(Context context) {
		super(context, _dbName, null, _dbVersion);
		_context = context;
		Log.i("DATABASECONTROLLER", "CONSTRUCTOR #1");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i("DATABASECONTROLLER", "onCreate");
		_database = db;
		// Need to check if the tables exist. If they don't, then create them
		createWorkoutsTable(db);
		createLocationsTable(db);
	}

	private void createSplitsTable(SQLiteDatabase db) {
		String createstr = "CREATE TABLE IF NOT EXISTS " + _splitsTableName
				+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT,ID_WORKOUT INTEGER,TIME INTEGER, DISTANCE REAL,"
				+ "FOREIGN KEY (ID_WORKOUT) REFERENCES WORKOUTS(ID) ON DELETE CASCADE)";
		db.execSQL(createstr);

		if (tableExists(_splitsTableName, false)) {
			Log.i("DATABASECONTROLLER", "Splits Table exists");
		} else {
			Log.i("DATABASECONTROLLER", "Splits Table does not exist");
		}
	}

	private void createWorkoutsTable(SQLiteDatabase db) {
		String createstr = "CREATE TABLE IF NOT EXISTS " + _workoutsTableName
				+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ACTIVE INTEGER, STARTTIME INTEGER, ENDTIME INTEGER, PAUSEDTIME INTEGER, DISTANCE REAL, UPLOADED INTEGER)";

		db.execSQL(createstr);

		// Check if the table exists
		if (tableExists(_workoutsTableName, false)) {
			Log.i("DATABASECONTROLLER", "Workouts Table exists");
		} else {
			Log.i("DATABASECONTROLLER", "Workouts Table does not exist");
		}
	}

	private void createLocationsTable(SQLiteDatabase db) {
		String createstr = "CREATE TABLE IF NOT EXISTS "
				+ _locationsTableName
				+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT,ID_WORKOUT INTEGER,LAT REAL,LON REAL,TIME INTEGER, ACCURACY REAL,BEARING REAL, ALTITUDE REAL, SPLIT INTEGER, ACCUMDIST REAL, ELAPSEDTIME INTEGER,"
				+ " FOREIGN KEY (ID_WORKOUT) REFERENCES WORKOUTS(ID) ON DELETE CASCADE)";
		db.execSQL(createstr);
		// Check if the table exists
		if (tableExists(_locationsTableName, false)) {
			Log.i("DATABASECONTROLLER", "Locations Table exists");
		} else {
			Log.i("DATABASECONTROLLER", "Locations Table does not exist");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
	}

	public void addWorkout(Workout workout, boolean activeworkout) throws Exception {
		FileLogger.writeToFile("DatabaseController","+++++ DatabaseController addWorkout +++++");
		SQLiteDatabase db = getWritableDatabase();

		long workoutid;

		FileLogger.writeToFile("DatabaseController","starttime = " + workout.get_startTime());
		FileLogger.writeToFile("DatabaseController","endTime = " + workout.get_endTime());
		FileLogger.writeToFile("DatabaseController","pausedTime = " + workout.get_pausedTime());
		FileLogger.writeToFile("DatabaseController","distance = " + workout.get_distance());

		ContentValues cv = new ContentValues();
		cv.put("ACTIVE",activeworkout?1:0);
		cv.put("STARTTIME", workout.get_startTime());
		cv.put("ENDTIME", workout.get_endTime());
		cv.put("PAUSEDTIME", workout.get_pausedTime());
		cv.put("DISTANCE", workout.get_distance());
		cv.put("UPLOADED", "0");

		FileLogger.writeToFile("DatabaseController","Calling db.insert");

		workoutid = db.insert(_workoutsTableName, "null", cv);

		FileLogger.writeToFile("DatabaseController","workoutid = " + workoutid);

		// As long as we don't have a -1 for the workoutid, we are good to continue
		if (workoutid != -1) {
			// Do the same for the locations
			ArrayList<LocationInfo> locs = workout.getLocations();

			FileLogger.writeToFile("DatabaseController","Number of locations = " + locs.size());
			for (LocationInfo loc : locs) {
				addLocation(workoutid, loc);
			}
		} else {
			throw new Exception("Unable to insert data into workouts table");
		}

		db.close();
		FileLogger.writeToFile("DatabaseController","----- DatabaseController addWorkout -----");
	}

	public void addLocation(long workoutid, LocationInfo loc) throws Exception {
		FileLogger.writeToFile("DatabaseController","+++++ DatabaseController addLocation +++++");
		ContentValues cv = new ContentValues();
		cv.put("ID_WORKOUT", workoutid);
		cv.put("LAT", loc.getLatitude());
		cv.put("LON", loc.getLongitude());
		cv.put("TIME", loc.getTime());
		cv.put("ACCURACY", loc.getAccuracy());
		cv.put("BEARING", loc.getBearing());
		cv.put("ALTITUDE", loc.getAltitude());
		cv.put("SPLIT", loc.is_isSplit() ? 1 : 0);
		cv.put("ELAPSEDTIME", loc.get_elapsedTime());
		cv.put("ACCUMDIST", loc.get_accumulatedDistance());

		SQLiteDatabase db = getWritableDatabase();

		long locationid = db.insert(_locationsTableName, "null", cv);

		if (locationid == -1) {
			throw new Exception("Unable to insert record into locations table");
		}

		db.close();
		FileLogger.writeToFile("DatabaseController","----- DatabaseController addLocation -----");
	}

	public boolean tableExists(String tableName, boolean openDb) {
		Log.i("DATABASECONTROLLER", "tableExists");

		if (openDb) {
			if (_database == null || !_database.isOpen()) {
				_database = getReadableDatabase();
			}

			if (!_database.isReadOnly()) {
				_database.close();
				_database = getReadableDatabase();
			}
		}

		Cursor cursor = _database.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
		if (cursor != null) {
			if (cursor.getCount() > 0) {
				cursor.close();
				return true;
			}
			cursor.close();
		}
		return false;
	}

	public long getWorkoutRecordsCount() {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getReadableDatabase();
		return DatabaseUtils.queryNumEntries(db, _workoutsTableName);
	}

	public long getLocationRecordsCount() {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getReadableDatabase();
		return DatabaseUtils.queryNumEntries(db, _locationsTableName);
	}

	public long getSplitRecordsCount() {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getReadableDatabase();
		return DatabaseUtils.queryNumEntries(db, _splitsTableName);
	}

	private List<Long> getWorkouts() {

		String[] projection = { "ID" };

		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.query(true, _workoutsTableName, projection, null, null, null, null, "ID", null);

		List<Long> al = new ArrayList<Long>();

		while (c.moveToNext()) {
			long id = c.getLong(c.getColumnIndexOrThrow("ID"));

			// Need to verify that there are location records for this workout. If not, don't bother with it
			if (getLocationsCount(id) > 0) {
				al.add(id);
			}
		}

		return al;
	}

	private int getLocationsCount(long id) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(_locationsTableName, _locationProjection, "id_workout = " + id, null, null, null, null, null);

		if (c == null) {
			return 0;
		} else {
			return c.getCount();
		}
	}

	public List<Workout> getWorkoutData() {
		List<Workout> workouts = new ArrayList<Workout>();
		List<Long> workoutIDs = getWorkouts();

		for (Long id : workoutIDs) {
			try {
				Workout wrk = getWorkoutFromDB(id);
				workouts.add(wrk);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return workouts;
	}

	private Workout getLocationDataFromDB(Long workoutid, Workout workout) throws Exception {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(_locationsTableName, _locationProjection, "ID_WORKOUT = " + workoutid, null, null, null, null, null);

		if (c == null) {
			throw new Exception("No location data available for workout " + workoutid);
		}

		// All is well. Continue to load up the SimpleDB instance
		while (c.moveToNext()) {
			Location loc = new Location(LocationManager.GPS_PROVIDER);

			// Add location specific information
			loc.setAccuracy(c.getFloat(c.getColumnIndexOrThrow("ACCURACY")));
			loc.setAltitude(c.getDouble(c.getColumnIndexOrThrow("ALTITUDE")));
			loc.setBearing(c.getFloat(c.getColumnIndexOrThrow("BEARING")));
			// loc.set_isSplit());
			loc.setLatitude(c.getDouble(c.getColumnIndexOrThrow("LAT")));
			loc.setLongitude(c.getDouble(c.getColumnIndexOrThrow("LON")));
			// loc.set_splitDistance(c.getString(c.getColumnIndexOrThrow("ACCUMDIST")));
			loc.setTime(c.getLong(c.getColumnIndexOrThrow("TIME")));

			// Now that we have created the Location, we must put it into a LocationInfo
			LocationInfo locinfo = new LocationInfo(loc);
			locinfo.set_elapsedTime(c.getLong(c.getColumnIndexOrThrow("ELAPSEDTIME")));
			int issplit = c.getInt(c.getColumnIndexOrThrow("SPLIT"));

			locinfo.set_isSplit((issplit == 1) ? true : false);
			workout.addLocation(locinfo);
		}

		return workout;
	}

	private Workout getWorkoutFromDB(Long workoutid) throws Exception {

		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.query(false, _workoutsTableName, _workoutProjection, "id = " + workoutid, null, null, null, null, null);

		// Make sure the cursor returned something
		if (c == null) {
			throw new Exception("Something very wrong! Query did not return a workout for id " + workoutid);
		}

		// All is well, continue
		// Create the workout item
		Workout item = new Workout();

		// Populate it with the data from the database and return it
		c.moveToFirst();
		item.set_startTime(c.getLong(c.getColumnIndexOrThrow("STARTTIME")));

		item.set_endTime(c.getLong(c.getColumnIndexOrThrow("ENDTIME")));

		item.set_pausedTime(c.getInt(c.getColumnIndexOrThrow("PAUSEDTIME")));

		item.set_distance(c.getDouble(c.getColumnIndexOrThrow("DISTANCE")));

		FileLogger.writeToFile("DatabaseController","----- DatabaseController getWorkoutData -----");

		item = getLocationDataFromDB(workoutid, item);

		return item;
	}

	private SimpleDBWorkoutItem getWorkoutData(Long workoutid) throws Exception {
		FileLogger.writeToFile("DatabaseController","+++++ DatabaseController getWorkoutData +++++");
		String sortorder = "ID asc";

		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.query(false, _workoutsTableName, _workoutProjection, "id = " + workoutid, null, null, null, null, null);

		// Make sure the cursor returned something
		if (c == null) {
			throw new Exception("Something very wrong! Query did not return a workout for id " + workoutid);
		}

		// All is well, continue
		// Create the SimpleDBWorkoutItem
		SimpleDBWorkoutItem item = new SimpleDBWorkoutItem();

		// Populate it with the data from the database and return it
		item.set_workoutid(Long.toString(workoutid));

		c.moveToFirst();
		item.set_startTime(c.getString(c.getColumnIndexOrThrow("STARTTIME")));
		FileLogger.writeToFile("DatabaseController","startTime = " + c.getString(c.getColumnIndexOrThrow("STARTTIME")));

		item.set_endTime(c.getString(c.getColumnIndexOrThrow("ENDTIME")));
		FileLogger.writeToFile("DatabaseController","endTime = " + c.getString(c.getColumnIndexOrThrow("ENDTIME")));

		item.set_pausedTime(c.getString(c.getColumnIndexOrThrow("PAUSEDTIME")));
		FileLogger.writeToFile("DatabaseController","pausedTime = " + c.getString(c.getColumnIndexOrThrow("PAUSEDTIME")));

		item.set_distance(c.getString(c.getColumnIndexOrThrow("DISTANCE")));
		FileLogger.writeToFile("DatabaseController","distance = " + c.getString(c.getColumnIndexOrThrow("DISTANCE")));

		FileLogger.writeToFile("DatabaseController","----- DatabaseController getWorkoutData -----");

		return item;
	}

	private ArrayList<SimpleDBWorkoutItem> getLocationData(SimpleDBWorkoutItem item, Long workoutid) throws Exception {
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(_locationsTableName, _locationProjection, "ID_WORKOUT = " + workoutid, null, null, null, null, null);

		if (c == null) {
			throw new Exception("No location data available for workout " + workoutid);
		}

		// First, extract the workout high level info from the workoutitem passed in
		String starttime = item.get_startTime();
		String pausedtime = item.get_pausedTime();
		String endtime = item.get_endTime();
		String distance = item.get_distance();

		ArrayList<SimpleDBWorkoutItem> itemlist = new ArrayList<SimpleDBWorkoutItem>();

		// All is well. Continue to load up the SimpleDB instance
		while (c.moveToNext()) {
			// Create a new SImpleDBWorkoutItem
			SimpleDBWorkoutItem wItem = new SimpleDBWorkoutItem();

			// Top level workout stuff
			wItem.set_workoutid(Long.toString(workoutid));
			wItem.set_startTime(starttime);
			wItem.set_pausedTime(pausedtime);
			wItem.set_endTime(endtime);
			wItem.set_distance(distance);

			// Add location specific information
			wItem.set_accuracy(c.getString(c.getColumnIndexOrThrow("ACCURACY")));
			wItem.set_altitude(c.getString(c.getColumnIndexOrThrow("ALTITUDE")));
			wItem.set_bearing(c.getString(c.getColumnIndexOrThrow("BEARING")));
			wItem.set_elapsedTime(c.getString(c.getColumnIndexOrThrow("ELAPSEDTIME")));
			wItem.set_isSplit(c.getString(c.getColumnIndexOrThrow("SPLIT")));
			wItem.set_lat(c.getString(c.getColumnIndexOrThrow("LAT")));
			wItem.set_lon(c.getString(c.getColumnIndexOrThrow("LON")));
			wItem.set_splitDistance(c.getString(c.getColumnIndexOrThrow("ACCUMDIST")));
			wItem.set_time(c.getString(c.getColumnIndexOrThrow("TIME")));

			// Once we have added everything, we need to add this to the ArrayList
			itemlist.add(wItem);
		}

		return itemlist;
	}

	public List<SimpleDBWorkoutItem> getWorkoutDataForCloud(Long wrkid) throws Exception {
		// List<Long> workoutIDs = getWorkoutsToSync();
		// List<>

		// for (Long id : workoutIDs) {
		SimpleDBWorkoutItem wItem = getWorkoutData(wrkid);
		List<SimpleDBWorkoutItem> item = getLocationData(wItem, wrkid);

		// Save this to Amazon
		// SimpleDBController sdbController = new SimpleDBController();
		// sdbController.addWorkouttoSimpleDB(items);
		// }

		return item;
	}

	public List<Long> getWorkoutsToSync() {
		String[] projection = { "ID" };

		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.query(true, _workoutsTableName, projection, "UPLOADED = 0", null, null, null, "ID", null);

		List<Long> al = new ArrayList<Long>();

		while (c.moveToNext()) {
			long id = c.getLong(c.getColumnIndexOrThrow("ID"));

			// Need to verify that there are location records for this workout. If not, don't bother with it
			if (getLocationsCount(id) > 0) {
				al.add(id);
			}
		}

		return al;
	}
}
