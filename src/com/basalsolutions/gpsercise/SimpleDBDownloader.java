package com.basalsolutions.gpsercise;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;

import android.os.AsyncTask;

public class SimpleDBDownloader extends AsyncTask<Void, Integer, ArrayList<SimpleDBWorkoutItem>> {

	private AmazonSimpleDBClient _sdbClient;

	@Override
	protected ArrayList<SimpleDBWorkoutItem> doInBackground(Void... params) {
		SelectResult sr = null;
		String nexttoken = null;
		List<Item> awsItems = new ArrayList<Item>();

		do {
			String query = "Select * from GPSercise";

			SelectRequest sreq = new SelectRequest(query, true);
			sreq.setNextToken(nexttoken);

			// Issue the request
			SelectResult sresult = _sdbClient.select(sreq);
			nexttoken = sresult.getNextToken();

			awsItems.addAll(sresult.getItems());

		} while (nexttoken != null);

		return createWorkoutItemsList(awsItems);
	}

	private ArrayList<SimpleDBWorkoutItem> createWorkoutItemsList(List<Item> awsItems) {
		ArrayList<SimpleDBWorkoutItem> wItems = new ArrayList<SimpleDBWorkoutItem>();

		// Iterate the list
		for (Item item : awsItems) {
			List<Attribute> attrs = item.getAttributes();

			// Need to new SimpleDBWorkoutItem class for every item returned
			SimpleDBWorkoutItem ite = new SimpleDBWorkoutItem();

			for (Attribute attr : attrs) {
				String value = attr.getValue();
				String name = attr.getName();
				// Need to do a bunch of ifs. totally ridiculous
				if (name.compareTo("workout") == 0) {
					ite.set_workoutid(value);
				} else if (name.compareTo("starttime") == 0) {
					ite.set_startTime(value);
				} else if (name.compareTo("endtime") == 0) {
					ite.set_endTime(value);
				} else if (name.compareTo("pausedtime") == 0) {
					ite.set_pausedTime(value);
				} else if (name.compareTo("distance") == 0) {
					ite.set_distance(value);
				} else if (name.compareTo("latitude") == 0) {
					ite.set_lat(value);
				} else if (name.compareTo("longitude") == 0) {
					ite.set_lon(value);
				} else if (name.compareTo("time") == 0) {
					ite.set_time(value);
				} else if (name.compareTo("accuracy") == 0) {
					ite.set_accuracy(value);
				} else if (name.compareTo("bearing") == 0) {
					ite.set_bearing(value);
				} else if (name.compareTo("altitude") == 0) {
					ite.set_altitude(value);
				} else if (name.compareTo("split") == 0) {
					ite.set_isSplit(value);
				} else if (name.compareTo("splitdistance") == 0) {
					ite.set_lat(value);
				} else if (name.compareTo("elapsedtime") == 0) {
					ite.set_elapsedTime(value);
				}
			}
			
			wItems.add(ite);		
		}

		return wItems;
	}

	public SimpleDBDownloader() {
		super();

		AWSCredentials creds = new BasicAWSCredentials("AKIAJYYZHGOHMHPCCXZA", "snDcjN4gOmZOLo6xJNE5spV5jIodBu9nhWoTpbs3");

		// Create the SimpleDB Client
		_sdbClient = new AmazonSimpleDBClient(creds);
	}

}
