package com.basalsolutions.gpsercise;

import android.location.Location;

public interface LocationListener {
	public abstract void onLocationData(Location location);
}
