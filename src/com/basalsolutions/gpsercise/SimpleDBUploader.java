package com.basalsolutions.gpsercise;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.BatchPutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.ReplaceableItem;

import android.os.AsyncTask;

public class SimpleDBUploader extends AsyncTask<List<SimpleDBWorkoutItem>, Void, String> {

	private AmazonSimpleDBClient _sdbClient;
	private String _domain;
	private static final int _MAXITEMS = 25;
    private SimpleDBListener _simpleDBListener;
    
	public SimpleDBUploader(String domain) {
		super();

		AWSCredentials creds = new BasicAWSCredentials("AKIAJYYZHGOHMHPCCXZA", "snDcjN4gOmZOLo6xJNE5spV5jIodBu9nhWoTpbs3");

		// Create the SimpleDB Client
		_sdbClient = new AmazonSimpleDBClient(creds);
	}

	@Override
	protected String doInBackground(List<SimpleDBWorkoutItem>... params) {
		List<SimpleDBWorkoutItem> workouts = params[0];

		String workoutid = "-1";
		
		ArrayList<ReplaceableItem> riList = new ArrayList<ReplaceableItem>();

		int idx = 0;

		for (SimpleDBWorkoutItem workout : workouts) {
            workoutid = workout.get_workoutid();
			idx++;

			ReplaceableItem ri = addItem(workout, idx, true);

			riList.add(ri);

			if ((riList.size() % 25) == 0) {
				putSimpleDBItems(riList);
				riList.clear();
			}
		}

		// If we have more items to send, then send them
		if (riList.size() > 0) {
			putSimpleDBItems(riList);
		}

		return workoutid;
	}

	private void putSimpleDBItems(ArrayList<ReplaceableItem> riList) {
		BatchPutAttributesRequest bpar = new BatchPutAttributesRequest(_domain, riList);

		try {
			_sdbClient.batchPutAttributes(bpar);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		// Need to notify the listener
	}

	// This adds just one location
	private ReplaceableItem addItem(SimpleDBWorkoutItem workoutitem, int idx, boolean replace) {
		ReplaceableAttribute wrkout = new ReplaceableAttribute("workout", workoutitem.get_workoutid(), replace);
		ReplaceableAttribute wrkstart = new ReplaceableAttribute("starttime", workoutitem.get_startTime(), replace);
		ReplaceableAttribute wrkend = new ReplaceableAttribute("endtime", workoutitem.get_endTime(), replace);
		ReplaceableAttribute wrkpause = new ReplaceableAttribute("pausedtime", workoutitem.get_pausedTime(), replace);		
		ReplaceableAttribute dist = new ReplaceableAttribute("distance", workoutitem.get_distance(), replace);
		ReplaceableAttribute lat = new ReplaceableAttribute("latitude", workoutitem.get_lat(), replace);
		ReplaceableAttribute lon = new ReplaceableAttribute("longitude", workoutitem.get_lon(), replace);
		ReplaceableAttribute time = new ReplaceableAttribute("time", workoutitem.get_time(), replace);
		ReplaceableAttribute acc = new ReplaceableAttribute("accuracy", workoutitem.get_accuracy(), replace);
		ReplaceableAttribute bear = new ReplaceableAttribute("bearing", workoutitem.get_bearing(), replace);
		ReplaceableAttribute alt = new ReplaceableAttribute("altitude", workoutitem.get_altitude(), replace);
		ReplaceableAttribute split = new ReplaceableAttribute("split", workoutitem.get_isSplit(), replace);
		ReplaceableAttribute splitdist = new ReplaceableAttribute("splitdistance", workoutitem.get_splitDistance(), replace);
		ReplaceableAttribute elapsedtime = new ReplaceableAttribute("elapsedtime", workoutitem.get_elapsedTime(), replace);

		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(14);

		attrs.add(wrkout);
		attrs.add(wrkstart);
		attrs.add(wrkpause);
		attrs.add(wrkend);
		attrs.add(dist);
		attrs.add(lat);
		attrs.add(lon);
		attrs.add(time);
		attrs.add(acc);
		attrs.add(bear);
		attrs.add(alt);
		attrs.add(split);
		attrs.add(splitdist);
		attrs.add(elapsedtime);

		ReplaceableItem ri = new ReplaceableItem(workoutitem.get_workoutid() + "_" + idx);
		ri.withAttributes(attrs);

		return ri;
	}

	public SimpleDBListener get_simpleDBListener() {
		return _simpleDBListener;
	}

	public void set_simpleDBListener(SimpleDBListener _simpleDBListener) {
		this._simpleDBListener = _simpleDBListener;
	}

}
