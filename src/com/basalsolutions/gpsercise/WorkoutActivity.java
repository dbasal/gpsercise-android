package com.basalsolutions.gpsercise;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class WorkoutActivity extends Activity {
	private GoogleMap _map = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_workout);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.workout, menu);
		return true;
	}

	
	private void setup() {
		// Check and make sure we need to get the map
		if (_map == null) {
			_map = ((MapFragment) getFragmentManager().findFragmentById(R.id.workoutemap)).getMap();

			if (_map == null) {
				try {
					throw new Exception("FUCK YOU. MAP IS NULL");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		// Need to set the current position of the map
		_map.setMyLocationEnabled(false);
	}
}
