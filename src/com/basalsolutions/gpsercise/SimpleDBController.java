package com.basalsolutions.gpsercise;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.regions.Region;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.BatchPutAttributesRequest;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.DeleteDomainRequest;
import com.amazonaws.services.simpledb.model.GetAttributesRequest;
import com.amazonaws.services.simpledb.model.GetAttributesResult;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.ReplaceableItem;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;
import com.amazonaws.services.simpledb.util.SimpleDBUtils;

public class SimpleDBController implements SimpleDBListener{

	private String _domain;
	private AmazonSimpleDBClient _sdbClient;
	private SimpleDBListener _simpleDBListener;

	public SimpleDBController(String domain) {
		this.set_domain(domain);
	}

	public String get_domain() {
		return _domain;
	}

	public void set_domain(String _domain) {
		this._domain = _domain;
	}

	// ******** WORKING ON THIS ***********
	public void addWorkouttoSimpleDB(List<SimpleDBWorkoutItem> workoutitems) {
		// Create the AsyncTask
		SimpleDBUploader uploader = new SimpleDBUploader(_domain);
		uploader.set_simpleDBListener(this);
	//	as
		uploader.execute(workoutitems);
	}

	public void getWorkoutfromSimpleDB(String workoutid) {
		String nextToken = null;
		
		do {
			SelectRequest sr = new SelectRequest("select * from GPSercise where workout = " + workoutid).withConsistentRead(true);
			
			if (nextToken != null) {
				sr.setNextToken(nextToken);
			}
			
			SelectResult result = _sdbClient.select(sr);
			
		} while (true);
	}

	@Override
	public void onUploadDataComplete(String workoutid) {
Log.i("SIMPLEDBCONTROLLER","Workout Uploaded : " + workoutid);		
	}

	public SimpleDBListener get_simpleDBListener() {
		return _simpleDBListener;
	}

	public void set_simpleDBListener(SimpleDBListener _simpleDBListener) {
		this._simpleDBListener = _simpleDBListener;
	}
}
