package com.basalsolutions.gpsercise;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.text.*;

import android.os.Environment;
import android.util.Log;
public class FileLogger {

	private static BufferedWriter _logWriter;

	public FileLogger() {
		// TODO Auto-generated constructor stub
	}

	public static void CreateLogFile(boolean append) throws IOException {
		File Root = Environment.getExternalStorageDirectory();
		if (Root.canWrite()) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dte = sdf.format(new Date());
			File LogFile = new File(Root, "RunLog-" + dte + ".txt");
			FileWriter LogWriter = new FileWriter(LogFile, append);
			_logWriter = new BufferedWriter(LogWriter);
			Date date = new Date();
			_logWriter.write("Logfile created at " + String.valueOf(date.toString() + "\n"));
		}
	}

	public static void writeToFile(String tag, String message) {
		if (message != null) {
			try {
				Date date = new Date();
				
				if (_logWriter != null) {
					_logWriter.write(String.valueOf(date.toString()) + " : " + message + "\n");
					_logWriter.flush();
				} else {
					CreateLogFile(true);
					writeToFile("FileLogger","_logWriter was null");
					writeToFile(tag,message);
				}
			} 
			  catch (IOException e) {
				e.printStackTrace();
			}
			
			Log.i(tag, message);
		}
	}

	public static void closeFile() {
		if (_logWriter != null) {
			try {
				_logWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
