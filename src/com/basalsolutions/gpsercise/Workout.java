package com.basalsolutions.gpsercise;

import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class Workout implements Parcelable {

	private long _startTime = 0;
	private long _endTime = 0;
	private long _pausedTime = 0;
	private double _distance = 0;
	private double _averageSpeed = 0;
	private long _elapsedTime = 0;
	private long _baseTime = 0;
	private long _splitBaseTime = 0;

	private ArrayList<LocationInfo> _locations;

	public long get_baseTime() {
		return _baseTime;
	}

	public void set_baseTime(long _baseTime) {
		this._baseTime = _baseTime;
	}

	public long get_splitBaseTime() {
		return _splitBaseTime;
	}

	public void set_splitBaseTime(long _splitBaseTime) {
		this._splitBaseTime = _splitBaseTime;
	}

	public double get_averageSpeed() {
		return _averageSpeed;
	}

	public void set_averageSpeed(double _averageSpeed) {
		this._averageSpeed = _averageSpeed;
	}

	public Workout() {
		_locations = new ArrayList<LocationInfo>();
	}

	@SuppressWarnings("unchecked")
	private Workout(Parcel in) {
		this._startTime = in.readLong();
		this._endTime = in.readLong();
		this._pausedTime = in.readInt();
		this._distance = in.readDouble();
		this._averageSpeed = in.readDouble();
		this._elapsedTime = in.readLong();
		this._baseTime = in.readLong();
		this._splitBaseTime = in.readLong();
		this._locations = (ArrayList<LocationInfo>) in.readArrayList(LocationInfo.class.getClassLoader());
	}

	public long get_startTime() {
		return _startTime;
	}

	public void set_startTime(long _startTime) {
		this._startTime = _startTime;

		long millis = SystemClock.elapsedRealtime();

		this._baseTime = millis;
		this._splitBaseTime = millis;
	}

	public long get_endTime() {
		return _endTime;
	}

	public void set_endTime(long _endTime) {
		this._endTime = _endTime;
	}

	public long get_pausedTime() {
		return _pausedTime;
	}

	public void set_pausedTime(int _pausedTime) {
		this._pausedTime = _pausedTime;
	}

	public double get_distance() {
		return _distance;
	}

	public void set_distance(double distance) {
		this._distance += distance;
	}

	public void addLocation(LocationInfo loc) {
		if (loc != null) {
			_locations.add(loc);

			// If this is the first location, then we need to set the starttime
			if (_locations.size() == 1) {
				Date dt = new Date();

				this.set_startTime(dt.getTime());
			}
		}
	}

	public LocationInfo getLocation(int index) {
		return _locations.get(index);
	}

	public ArrayList<LocationInfo> getLocations() {
		return _locations;
	}

	public int get_numLocations() {
		if (_locations != null) {
			return _locations.size();
		} else {
			return 0;
		}
	}

	public LocationInfo getLastLocation() {
		if (_locations.size() == 0) {
			return null;
		} else {
			return _locations.get(_locations.size() - 1);
		}
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(_startTime);
		dest.writeLong(_endTime);
		dest.writeLong(_pausedTime);
		dest.writeDouble(_distance);
		dest.writeDouble(_averageSpeed);
		dest.writeLong(_elapsedTime);
		dest.writeLong(_baseTime);
		dest.writeLong(_splitBaseTime);
		dest.writeList(_locations);
	}

	public static final Parcelable.Creator<Workout> CREATOR = new Parcelable.Creator<Workout>() {

		@Override
		public Workout createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Workout(source);
		}

		@Override
		public Workout[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Workout[size];
		}
	};
}
