package com.basalsolutions.gpsercise;

import java.util.List;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;

import android.content.Context;
import android.util.Log;

public class AWSController implements SimpleDBListener {

	private Context _context;
	private SimpleDBController _simpleDBController;
	private SimpleDBListener _simpleDBListener;
	private DatabaseController _dbController;
	private String _sdbDomain = "GPSercise";

	AWSCredentials _creds = new BasicAWSCredentials("AKIAIQWEAMTR4YPTBRCQ", "Sk7lW57bpVa5hwKjrps5bCoptgyU1SoHhDEcARxa");
	
	public AWSController(Context context) {
		_context = context;
	}

	public void saveToCloud() {
		_dbController = new DatabaseController(_context);
		
		_simpleDBController = new SimpleDBController(_sdbDomain);
		_simpleDBController.set_simpleDBListener(this);
		
		// need to get all the workout ids that we need to save
		List<Long> ids = _dbController.getWorkoutsToSync();
		
		for (Long id : ids) {
			try {
				List<SimpleDBWorkoutItem> workout = _dbController.getWorkoutDataForCloud(id);
				
				_simpleDBController.addWorkouttoSimpleDB(workout);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onUploadDataComplete(String workoutid) {
		// TODO Auto-generated method stub
		Log.i("AWSCONTROLLER","Workout Uploaded : " + workoutid);		
		
	}

}
